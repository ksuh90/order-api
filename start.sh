#!/bin/bash

# Install docker
sudo apt-get update && \
sudo apt-get -y install apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common && \
sudo curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; sudo apt-key add /tmp/dkey && \
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
   $(lsb_release -cs) \
   stable" && \
sudo apt-get update && \
sudo apt-get -y install docker-ce

# Install docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Install ruby
yes | sudo apt install ruby
yes | sudo apt-get install ruby-all-dev

# Install docker sync
sudo gem install docker-sync

# Spin up docker containers
echo "yes | sudo docker-sync start"
yes | sudo docker-sync start
echo "sudo docker-compose up -d"
sudo docker-compose up -d
sudo docker stop la-cloudant
sudo docker-compose up -d cloudant
until $(curl --output /dev/null --silent --head --fail http://0.0.0.0:5984); do
    printf '.'
    sleep 2
done
curl http://admin:pass@0.0.0.0:5984/order -X PUT

exit 0
