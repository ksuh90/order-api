const chai = require('chai');
const assert = chai.assert;
const Order = require('../../models/order');

describe('models/order', function() {

    it(`should create an order model`, function() {
        const order = new Order(123, 'UNASSIGN', 'aaa');
        assert.equal(order.distance, 123);
        assert.equal(order.status, 'UNASSIGN');
        assert.equal(order.id, 'aaa');
        assert.deepEqual(order.asJSON(), {
            distance: 123,
            status: 'UNASSIGN',
            id: 'aaa',
        });
    });

    it(`should set status to 'foo'`, function() {
        const order = new Order(123, 'UNASSIGN', 'aaa');
        order.status = 'foo';
        assert.equal(order.status, 'foo');
    });
});
