const chai = require('chai');
const Cloudant = require('@cloudant/cloudant');
const assert = chai.assert;
const request = require('../request');
const config = require('../../config');
const calcDistance = require('../../util/distance');

// Instantiate cloudant client
const cloudant = Cloudant({
    plugins: 'promises',
    url: config.cloudant.url,
    account: config.cloudant.username,
    password: config.cloudant.pass
});

describe('routes', function() {

    beforeEach(async function() {
        await cloudant.db.create(config.cloudant.dbname);
    });

    afterEach(async function() {
        await cloudant.db.destroy(config.cloudant.dbname);
    });

    describe('GET /', function() {
        it(`should respond with 200`, async function() {
            const resp = await request.get(`/`).expect(200);
        });
    });

    describe('POST /order', function() {
        it(`should create new order`, async function() {
            // Arrange
            const dist = await calcDistance([22.2860, 114.1915], [22.3081, 114.2019]);

            // Action
            const res = await request.post(`/order`).send({
                origin: [22.2860, 114.1915],
                destination: [22.3081, 114.2019]
            }).expect(200);

            // Assert
            const db = cloudant.db.use(config.cloudant.dbname);
            const doc = await db.get(res.body.id);
            assert.equal(doc._id, res.body.id);
            assert.equal(doc.distance, dist);
            assert.equal(doc.status, 'UNASSIGN');
        });

        it(`should respond with 500 for invalid payload`, async function() {
            const res = await request.post(`/order`).send({
                origin: [],
                destination: []
            }).expect(500);
            assert.isNotEmpty(res.error);
        });
    });

    describe('PUT /order/:id', function() {
        it(`should update status to SUCCESS`, async function() {
            // Arrange
            const db = cloudant.db.use(config.cloudant.dbname);
            const resInsert = await db.insert({
                _id: 'foobar',
                distance: 1234,
                status: 'UNASSIGN',
            });
                  
            // Action
            const res = await request.put(`/order/foobar`).send({ status: 'taken' }).expect(200);

            // Assert
            const doc = await db.get('foobar');
            assert.equal(doc.status, 'SUCCESS');
        });

        it(`should respond with 409 for order already taken`, async function() {
            // Arrange
            const db = cloudant.db.use(config.cloudant.dbname);
            const resInsert = await db.insert({
                _id: 'foobar',
                distance: 1234,
                status: 'SUCCESS',
            });

            // Action
            const res = await request.put(`/order/foobar`).send({ status: 'taken' }).expect(409);

            // Assert
            assert.equal(res.body.error, 'ORDER_ALREADY_BEEN_TAKEN');
        });

        it(`should respond with 404 for invalid order id`, async function() {
            // Action
            const res = await request.put(`/order/blah`).send({ status: 'taken' }).expect(404);

            // Assert
            assert.equal(res.body.error, 'ORDER_DOES_NOT_EXIST');
        });
    });

    describe('GET /orders', function() {
        it(`should get all orders`, async function() {
            // Arrange
            const bulk = [
                {
                    id: 'aaa',
                },
                {
                    id: 'bbb',
                },
            ];
            const db = cloudant.db.use(config.cloudant.dbname);
            const resBulk = await db.bulk({ docs: bulk });

            // Action
            const res = await request.get('/orders').expect(200);

            // Assert
            assert.equal(res.body.length, 2);
        });

        it(`should get first 2 orders`, async function() {
            // Arrange
            const bulk = [
                {
                    id: 'aaa',
                },
                {
                    id: 'bbb',
                },
                {
                    id: 'ccc',
                },
            ];
            const db = cloudant.db.use(config.cloudant.dbname);
            const resBulk = await db.bulk({ docs: bulk });

            // Action
            const res = await request.get('/orders?limit=2').expect(200);

            // Assert
            assert.equal(res.body.length, 2);
        });

        it(`should skip first 2 and get next 2 orders`, async function() {
            // Arrange
            const bulk = [
                {
                    id: 'aaa',
                },
                {
                    id: 'bbb',
                },
                {
                    id: 'ccc',
                },
                {
                    id: 'ddd',
                },
            ];
            const db = cloudant.db.use(config.cloudant.dbname);
            const resBulk = await db.bulk({ docs: bulk });

            // Action
            const res = await request.get('/orders?limit=2&page=2').expect(200);

            // Assert
            assert.equal(res.body.length, 2);
        });
    });
});
