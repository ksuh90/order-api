const chai = require('chai');
const assert = chai.assert;
const calcDistance = require('../../util/distance');

describe('util/distance', function() {
    it(`should return with a distance value of 10709`, async function() {
        const actual = await calcDistance([22.2860, 114.1915], [22.3081, 114.2019]);
        assert.equal(actual, 10709);
    });
});
