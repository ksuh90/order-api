const dev = {
    cloudant: {
        url: 'http://la-cloudant',
        dbname: 'order',
        username: 'admin',
        pass: 'pass'
    },
    google: {
        maps: {
            key: '<api key>'
        }
    }
};

const test = {
    cloudant: {
        url: 'http://la-cloudant',
        dbname: 'test',
        username: 'admin',
        pass: 'pass'
    },
    google: {
        maps: {
            key: '<api key>'
        }
    }
};

const prod = {
    // TODO
    cloudant: {
        url: '',
        dbname: '',
        username: '',
        pass: ''
    },,
    google: {
        maps: {
            key: '<api key>'
        }
    }
};

const config = {
    dev,
    test,
    prod,
};

module.exports = config[process.env.NODE_ENV];
