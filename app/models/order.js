/**
 * Describes an Order object
 * @param {int|string} distance
 * @param {string} status
 * @param {string} id
 */
class Order {
    constructor(d, status, id) {
        this._distance = parseInt(d);
        this._status  = status;
        this._id = id || null;
    }

    get distance() {
        return this._distance;
    }

    get status() {
        return this._status;
    }

    set status(s) {
        this._status = s;
    }

    get id() {
        return this._id;
    }

    set id(id) {
        this._id = id;
    }

    asJSON() {
        let ret = {};
        if (this._id) {
            ret['id'] = this._id;
        }
        ret['distance'] = this._distance;
        ret['status'] = this._status;        
        return ret;
    }
}

module.exports = Order;
