const config = require('../config');
const Order = require('../models/order');
const errorHandler = require('../util/error');
const cloudantdb = require('../util/db');

const take = async function(req, res, next) {
    const db = cloudantdb(config.cloudant.dbname);
    let doc = null;

    // Get the order
    try {
        doc = await db.get(req.params.id);
    } catch(err) {
        if (err.statusCode == 404) {
            return errorHandler(res, next, err.statusCode, 'ORDER_DOES_NOT_EXIST');
        }
    }

    let order = new Order(doc.distance, doc.status, doc._id);
    const succes = 'SUCCESS';

    // Handle update
    if (doc.status == succes) {
        return errorHandler(res, next, 409, 'ORDER_ALREADY_BEEN_TAKEN');
    } else if(req.body.status == 'taken') {
        order.status = succes;
        let newDoc = order.asJSON();
        newDoc['_rev'] = doc._rev;
        newDoc['_id'] = doc._id;
        delete newDoc['id'];
        const resp = await db.insert(newDoc);
        res.send({ status: succes });
    } else {
        return errorHandler(res, next, 400, 'UNRECOGNIZED_STATUS_REQUEST');
    }
    
    return next();
};

module.exports = take;
