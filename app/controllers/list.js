const config = require('../config');
const Order = require('../models/order');
const cloudantdb = require('../util/db');

const list = async function(req, res, next) {
    const db = cloudantdb(config.cloudant.dbname);
    const params = { include_docs: true };
    if (req.params.limit) {
        params['limit'] = req.params.limit;
        if (req.params.page) {
            params['skip'] = parseInt(req.params.limit) * (parseInt(req.params.page)-1);
        }
    } 

    const resp = await db.get('_all_docs', params);
    let orders = [];
    for (var i = 0; i < resp.rows.length; i++) {
        let doc = resp.rows[i].doc;
        doc['id'] = doc._id;
        delete doc['_rev'];
        delete doc['_id'];
        orders.push(doc);
    }
    res.send(orders);

    return next();
};

module.exports = list;
