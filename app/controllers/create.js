const config = require('../config');
const Order = require('../models/order');
const calcDistance = require('../util/distance');
const errorHandler = require('../util/error');
const cloudantdb = require('../util/db');

const create = async function(req, res, next) {
    const db = cloudantdb(config.cloudant.dbname);
    let distance = null;

    // Get the distance
    try {
        distance = await calcDistance(req.body.origin, req.body.destination);
    } catch(err) {
        return errorHandler(res, next, 500, err.message);
    }

    // Insert the new order to db
    try {
        const order = new Order(distance, 'UNASSIGN');
        const resInsert = await db.insert(order.asJSON());
        order.id = resInsert.id;
        res.send(order.asJSON());
    } catch(err) {
        return errorHandler(res, next, err.statusCode, 'Cloudant: ' + err.message);
    }

    return next();
};

module.exports = create;
