const version = require('../package.json').version;
const create = require('../controllers/create');
const take = require('../controllers/take');
const list = require('../controllers/list');

module.exports = function(server) {
    server.get('/', function(req, res, next){
        res.send({
            name: 'Order API',
            version: version,
            tagline: 'hello world!'
        });
        return next();
    });

    server.post('/order', create);
    server.put('/order/:id', take);
    server.get('/orders', list);
};
