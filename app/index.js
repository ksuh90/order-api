const restify = require('restify');

// Instantiate Restify server
const server = restify.createServer();
server.use(restify.plugins.queryParser({ mapParams: true }));
server.use(restify.plugins.bodyParser());
server.use(restify.plugins.fullResponse()); // sets up all of the default headers for the system

require('./routes')(server);

if (!module.parent){
    server.listen(8080, function() {
        console.log('%s listening at %s', server.name, server.url);
    });
}

module.exports = server;
