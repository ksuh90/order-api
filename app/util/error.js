const handler = function(res, next, status, msg) {
    res.send(status, {
        error: msg
    });
    return next();
}

module.exports = handler;
