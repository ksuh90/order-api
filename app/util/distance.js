const googlemaps = require('@google/maps');
const config = require('../config');

const latLng = function(lat, lng) {
    return {
        lat: lat,
        lng: lng
    };
};

/**
 * Get the distance via google maps api
 * @param  {int[]} origin [<lat>, <lng>]
 * @param  {int[]} dest   [<lat>, <lng>]
 * @return {int}
 */
const distance = async function(origin, dest) {
    const googleMapsClient = googlemaps.createClient({
        key: config.google.maps.key,
        Promise: Promise
    });
    const resp = await googleMapsClient.distanceMatrix({
        origins: [ latLng(origin[0], origin[1]) ],
        destinations: [ latLng(dest[0], dest[1]) ]
    }).asPromise();
    const status = resp.json.rows[0].elements[0].status;
    if (status != 'OK') {
        throw new Error('google maps api: ' + status);
    }
    return resp.json.rows[0].elements[0].distance.value;
};

module.exports = distance;
