const Cloudant = require('@cloudant/cloudant');
const config = require('../config');

const db = function(dbname) {
    return Cloudant({
        plugins: 'promises',
        url: config.cloudant.url,
        account: config.cloudant.username,
        password: config.cloudant.pass
    }).db.use(dbname);
};

module.exports = db;
