
# Order API
## How to run
1. Clone the repository
    ```
    $ git clone https://ksuh90@bitbucket.org/ksuh90/order-api.git
    ```
2. Copy ./app/config-sample.js and save as ./app/config.js
3. Set the google map api key fields in ./app/config.js
4. Run the start script from the root of the repository: ```sh start.sh```

## Tests
You can run tests from inside the container via ```npm test``` or outside the container with ```docker exec la-order-api npm test```

## Specs
### Nodejs
The api is built in Nodejs using the Restify framework.

### Cloudant
Cloudant is a fork of Apache Couchdb. It is a nosql document-store database. This database runs under the container name "la-cloudant". You may also access the dashboard with http://0.0.0.0:5984/dashboard.html

### Google Map API
The distance calculation is achieved through google map's distance matrix service. 
